package com.softtek.academy.IntegrationTest;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.softtek.academy.Proyecto2Application;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes =  Proyecto2Application.class )
public class IntegrationTest {

	@LocalServerPort
    private int port;
    
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    
    @Test
    public void testCreateStudent() throws Exception {
    	//Setup
    	String expectedMonth = "1";
        String expectedStatus = "200"; //Es el estado que arroja el Body si la peticion se hizo de forma correcta
    	//Execute
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("api/v1/period/" + expectedMonth), HttpMethod.GET, entity, String.class);
        //Verify
        JSONParser parser = new JSONParser();
        System.out.println(response.getBody());
        JSONObject json = (JSONObject) parser.parse(response.getBody());
        assertEquals(expectedStatus, json.get("typeResponse"));
        }    
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
}
