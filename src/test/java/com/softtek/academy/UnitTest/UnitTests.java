package com.softtek.academy.UnitTest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.softtek.academy.GetMonthInfoFromExcel;
import com.softtek.academy.Proyecto2Application;
import com.softtek.academy.models.Day;
import com.softtek.academy.models.Month;
import com.softtek.academy.repositories.DayRepository;
import com.softtek.academy.repositories.MonthRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Proyecto2Application.class)
public class UnitTests {

	@Autowired
    private MonthRepository monthRepository;
    
    @Autowired
    private DayRepository dayRepository;
    
    private static final String FILE_NAME = "Registros Momentum.xls";
    
    @Test 
    public void testFileReading() {
        System.out.println("Testea que el archivo este en la carpeta");
        File file = new File(FILE_NAME);
        assertTrue(file.exists());
    }
    
    @Test
    public void testGetAllLists() throws ParseException, IOException {
        //setup
        System.out.println("Testea que las listas no esten vacias obtenidas del excel");
        GetMonthInfoFromExcel excelrepository = new GetMonthInfoFromExcel();
        Object[] ret = excelrepository.setMonthInfo();
        List<Day> days = (List<Day>) ret[0];
        List<Month> meses = (List<Month>) ret[1];
        //verify
        assertNotNull(days);
        System.out.println(days.get(20).toString());
        assertNotNull(meses);
    }
    
    @Test
    public void findIsTestMonth() {
        //Setup
        System.out.println("Testea que el metodo JPA funcione correctamente");
        Month expectedMonth = new Month();
        expectedMonth.setIsstr("CXMG");
        //execute
        Month currentMonth = monthRepository.findFirstByIsstr("CXMG");
        System.out.println(currentMonth.toString());
        //verify
        assertNotNull(currentMonth);
        assertEquals(currentMonth.getIsstr(), expectedMonth.getIsstr());
    }
    
    @Test 
    public void checkDay(){
        System.out.println("Testea que exista conexion con la tabla DAY");
        //Setup
        //Execute
        List<Day> days = dayRepository.findAll();
        //Verify
        assertNotNull(days);
    }
    
	

}
