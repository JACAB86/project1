<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Person List</title>
  </head>
  <body>
    <h1>List By Month</h1>
    <a href="/view/api/v1/Principal">Volver</a>
    <br/><br/>
    <div>
      <table border="1">
        <tr>
          <th>IS</th>
          <th>Date</th>
          <th>Hours</th>
          
          
        </tr>
        <tr>
          <td>${months.isstr}</td>
          <td>${months.imes}</td>
	      <td>${months.hours}</td>				
        </tr>
      </table>
    </div>
  </body>
  
</html>