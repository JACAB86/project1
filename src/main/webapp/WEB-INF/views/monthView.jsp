<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Person List</title>
  </head>
  <body>
    <h1>List By Month</h1>
    <a href="/view/api/v1/Principal">Volver</a>
    <br/><br/>
    <div>
      <table border="1">
        <tr>
          <th>IS</th>
          <th>Date</th>
          <th>Hours</th>
          
          
        </tr>
        <c:forEach  items="${months}" var ="month">
        <tr>
          <td>${month.isstr}</td>
          <td>${month.imes}</td>
	      <td>${month.hours}</td>				
        </tr>
        </c:forEach>
      </table>
    </div>
  </body>
  
</html>