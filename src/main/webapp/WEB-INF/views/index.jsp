<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
<!DOCTYPE HTML>

<html>
<head></head>
<body>
<div>
<h5>Obten las horas del IS del usuario del mes introducido.</h5>
    <form action="/view/api/v1/users" method="get"/>  
        IS        : <input type="text" name="isstr"/>
        Month     : <input type="text" name="month"/>
        <input type="submit"  value="Buscar">
    </form>

</div>
<h5>Envia los parametros IS, startDate y EndDate para obtener todos los usuarios limitados por la fecha de inicio y fin.</h5>
    <form action="/view/api/v1/users" method="post"/>  
        Start Date: <input type="text" name="startDate"/>
        End Date  : <input type="text" name="endDate"/>
        IS  : <input type="text" name="is"/>
        <input type="submit"  value="Buscar">
   	</form>
		<br>
<div>
<h5>Obten todos los registros de usuarios del mes introducido.</h5>
    <form action="/view/api/v1/period" method="get"/>  
       	Month     : <input type="text" name="month"/>
        <input type="submit"  value="Buscar">
  	</form>
</div>	
<h5>Envia el parametro de Start Date y End Date para obtener todos los usuarios limitados por la fecha de inicio y fin.</h5>
    <form action="/view/api/v1/period" method="post"/>  
        Start Date: <input type="text" name="startDate"/>
        End Date  : <input type="text" name="endDate"/>
        <input type="submit"  value="Buscar">
    </form>  
    <br>
    <a href="/view/api/v1/Update">Update information from Excel</a>

</body>
</html>


