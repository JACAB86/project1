<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Person List</title>
  </head>
  <body>
    <h1>Person List</h1>
    
    <br/><br/>
    <div>
      <table border="1">
        <tr>
          <th>IS</th>
          <th>Date</th>
          <th>Hours</th>
          
          
        </tr>
        <c:forEach  items="${persons}" var ="person">
        <tr>
          <td>${person.isstr}</td>
          <td>${person.datestr}</td>
	      <td>${person.hours}</td>				
        </tr>
        </c:forEach>
      </table>
    </div>
  </body>
  
</html>