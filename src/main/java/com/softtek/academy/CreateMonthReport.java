package com.softtek.academy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.softtek.academy.models.Month;


public class CreateMonthReport {

	public void SetMonthInfoExcel(List<Month> listaMeses) throws IOException {
		//if(listaMeses.isEmpty()) { System.out.println("La lista esta vacia");}else {System.out.println("La lista no esta vacia");}
	
		try {
		
		
		//Blank workbook
        HSSFWorkbook workbook = new HSSFWorkbook(); 
         
        //Create a blank sheet
        Sheet sheet = workbook.createSheet("Employee Data");
		
        
        Row headerRow;
        Cell headerCell;
		headerRow = sheet.createRow(0);
		headerCell = headerRow.createCell(0);
		headerCell.setCellValue("Name");
		headerCell = headerRow.createCell(1);
		headerCell.setCellValue("Month");
		headerCell = headerRow.createCell(2);
		headerCell.setCellValue("Month Time");
		
		int i = 1;
		for(Month m : listaMeses) {
	        Row currentRow;
	        
			currentRow = sheet.createRow(i);
			
			Cell currentCell = currentRow.createCell(0);
			currentCell.setCellValue(m.getIsstr());
			
			currentCell = currentRow.createCell(1);
			currentCell.setCellValue(m.getImes());
			
			currentCell = currentRow.createCell(2);
			currentCell.setCellValue(m.getHours());//Si quieres obtener horas dividir entre 3600
			
			i++;
			
			}
		
			
        
        FileOutputStream out = new FileOutputStream(new File("Reporte.xls"));
        workbook.write(out);
        out.close();

        
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}
}
