package com.softtek.academy.services;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.CreateMonthReport;
import com.softtek.academy.GetMonthInfoFromExcel;
import com.softtek.academy.models.Day;
import com.softtek.academy.models.Month;
import com.softtek.academy.repositories.DayRepository;
import com.softtek.academy.repositories.MonthRepository;

@Service
public class UpdateServiceImpl implements UpdateService {

	@Autowired
	DayRepository dayRepository;
	@Autowired
	MonthRepository monthRepository;
	
	
	@Override
	public void updateAll() throws ParseException, IOException {
    	GetMonthInfoFromExcel info = new GetMonthInfoFromExcel();
    	System.out.println("Proceso de borrado y guardado");
    	Object[] obj = info.setMonthInfo();
    	List<Day> days = (List<Day>) obj[0];
    	List<Month> months = (List<Month>) obj[1];
    	System.out.println(months.size());
    	System.out.println(days.size());
    	// info.setMonthInfo(); //esta linea duplicaba la informacion
    	CreateMonthReport report = new CreateMonthReport();
    	report.SetMonthInfoExcel(months);
    	System.out.println(months.size());
    	System.out.println(days.size());
    	dayRepository.deleteAll();
    	dayRepository.saveAll(days);
    	monthRepository.deleteAll();
    	monthRepository.saveAll(months);
	}

}
