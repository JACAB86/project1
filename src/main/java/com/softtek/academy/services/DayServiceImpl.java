package com.softtek.academy.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.models.Day;
import com.softtek.academy.models.Response;
import com.softtek.academy.repositories.DayRepository;

@Service
public class DayServiceImpl implements DayService {

	@Autowired
	DayRepository dayRepository;

	@Override
	public Response getUsers(String is, String strDate, String endDate) {
		List<Day> d = dayRepository.findAllByIsstrAndDatestrBetween(is,strDate,endDate);
		Response res = new Response();
		if(!d.isEmpty()) { res.setResult(d); res.setTypeResponse("200");}
		else { 
			String error = "No existen datos para este usuario";
			Object obj= error;  
			res.setResult(obj); res.setTypeResponse("404");}
		return res;
	}

	@Override
	public Response getBetweenDates(String strDate, String endDate) {
		List<Day> d = dayRepository.findAllByDatestrBetween(strDate, endDate);
		Response res = new Response();
		if(!d.isEmpty()) { res.setResult(d); res.setTypeResponse("200");}
		else { 
			String error = "No existen datos para este usuario";
			Object obj= error;  
			res.setResult(obj); res.setTypeResponse("404");}
		return res;
	}

	@Override
	public List<Day> getAllUsers() {
		// TODO Auto-generated method stub
		return dayRepository.findAll();
	}

}
