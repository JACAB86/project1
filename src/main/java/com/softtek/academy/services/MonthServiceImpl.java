package com.softtek.academy.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.models.Month;
import com.softtek.academy.models.Response;
import com.softtek.academy.repositories.MonthRepository;

@Service
public class MonthServiceImpl implements MonthService {

	@Autowired
	MonthRepository monthRepository;
	
	@Override
	public Response getbyIsAndByIMonth(String isStr, Long iMonth) {
		Month m = monthRepository.findFirstByIsstrAndImes(isStr, iMonth);
		Response res = new Response();
		if(m != null) { res.setResult(m); res.setTypeResponse("200");}
		else { 
			String error = "No existen datos para este usuario";
			Object obj= error;  
			res.setResult(obj); res.setTypeResponse("404");}
		return res;
	}
	
	@Override
	public Response getAllByIMonth(Long iMonth){
		List<Month> m = monthRepository.findAllByImes(iMonth);
		Response res = new Response();
		if(!m.isEmpty()) { res.setResult(m); res.setTypeResponse("200");}
		else { 
			String error = "No existen datos para este usuario";
			Object obj= error;  
			res.setResult(obj); res.setTypeResponse("404");}
		return res;
	}



}
