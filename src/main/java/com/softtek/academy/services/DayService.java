package com.softtek.academy.services;

import java.util.List;

import com.softtek.academy.models.Day;
import com.softtek.academy.models.Response;

public interface DayService {

	Response getUsers(String is,String strDate,String endDate);
	Response getBetweenDates(String strDate,String endDate);
	List<Day> getAllUsers();
}
