package com.softtek.academy.services;

import java.io.IOException;
import java.text.ParseException;

public interface UpdateService {

	void updateAll() throws ParseException, IOException;

}
