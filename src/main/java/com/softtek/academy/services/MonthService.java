package com.softtek.academy.services;


import com.softtek.academy.models.Response;

public interface MonthService {
	
	Response getbyIsAndByIMonth(String isStr,Long iMonth);
	Response getAllByIMonth(Long month);

}
