package com.softtek.academy.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softtek.academy.models.Day;

@Repository
public interface DayRepository extends JpaRepository<Day, Long> {
	
	Day findFirstByIsstr(String isstr);
	List<Day> findAllByDatestrBetween(String strDate,String endDate);
	List<Day> findAllByIsstrAndDatestrBetween(String is, String strDate,String endDate);
	
}
