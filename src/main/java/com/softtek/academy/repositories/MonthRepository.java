package com.softtek.academy.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softtek.academy.models.Month;

@Repository
public interface MonthRepository extends JpaRepository<Month, Long> {

	Month findFirstByIsstr(String isstr);
	Month findFirstByIsstrAndImes(String isstr, Long month);
	List<Month> findAllByImes(Long month);
	
}
