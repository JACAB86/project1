package com.softtek.academy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.softtek.academy.models.Day;
import com.softtek.academy.models.Month;


public class GetMonthInfoFromExcel {
	

	List<Day> listaDias = new ArrayList<>();
	List<Month> listaMeses = new ArrayList<>();
	Day objetoAuxiliarDia = new Day();
	Date currentDate;
	Date hours;
	int imes,iNewMes;
	long currentSeconds;
	long sumSeconds;
	int auxSegundos;
	String Hours;

	public Object[] setMonthInfo() throws ParseException, IOException{
		GetDayInfoFromExcel info = new GetDayInfoFromExcel();
		listaDias = info.getAllDays();
		

		Iterator<Day> iteradorDias = listaDias.iterator();
		objetoAuxiliarDia = iteradorDias.next();
		while(iteradorDias.hasNext()) {
			Month objetoAuxiliarMes = new Month();
		    String auxIs;
		    objetoAuxiliarMes.setIsstr(objetoAuxiliarDia.getIsstr());//Month.Is
		    
		    currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(objetoAuxiliarDia.getDatestr());
		    imes = currentDate.getMonth() + 1 ;
		    iNewMes = imes;
		    //sumSeconds = sumSeconds + objetoAuxiliarDia.getSeconds();
		    //System.out.println(imes);
			sumSeconds = 0;
			auxIs = objetoAuxiliarDia.getIsstr();
		    while(iNewMes == imes && auxIs.equals(objetoAuxiliarDia.getIsstr())) {
		    	hours =	new SimpleDateFormat("hh:mm:ss").parse(objetoAuxiliarDia.getHours());
		    	sumSeconds = sumSeconds + objetoAuxiliarDia.getSeconds(); //Cambie esta linea para que contara los segundos
			    if(!iteradorDias.hasNext()) {
			    		break;
			    }else {
			    	objetoAuxiliarDia = iteradorDias.next(); //Aqui ya cambia de objeto
			    	currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(objetoAuxiliarDia.getDatestr());
			    	iNewMes = currentDate.getMonth() + 1 ;
			    	}
		    }
		    objetoAuxiliarMes.setImes(imes);

            if((sumSeconds/3600)<10) 
            	if(((sumSeconds%3600)/60)<10) 
            		if(((sumSeconds%3600)%60)<10)
            			Hours= "0"+(sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            		else Hours= "0"+(sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            	else if(((sumSeconds%3600)%60)<10) 
            				Hours= "0"+(sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            			else Hours= "0"+(sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            else if(((sumSeconds%3600)/60)<10)
            		if(((sumSeconds%3600)%60)<10) Hours= (sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            		else Hours= (sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            	else if(((sumSeconds%3600)%60)<10) Hours= (sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            		else Hours= (sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            
		    objetoAuxiliarMes.setHours(Hours);
		    listaMeses.add(objetoAuxiliarMes);

		}

		//System.out.println(listaMeses.size());
		Object[] ret = {listaDias, listaMeses};
		return ret;
		
	}
}
