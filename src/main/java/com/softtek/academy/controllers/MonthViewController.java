package com.softtek.academy.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.models.Day;
import com.softtek.academy.models.Month;
import com.softtek.academy.models.Response;
import com.softtek.academy.services.MonthService;

@Controller
@RequestMapping("view/api/v1")
public class MonthViewController {
	
	//Acceder a traves de http://localhost:8080/view/api/v1/Principal
	
	@Autowired
	MonthService monthService;

	@Autowired
	MonthController monthController;
	
	@Autowired
	UpdateController updateController;
	
	
	
	Response response;
	
	@RequestMapping(path = "/Principal")
	public ModelAndView index() {
		return new ModelAndView("index");
	}
	
	@RequestMapping(path = "/Update")
	public ModelAndView update() throws ParseException, IOException {
		return new ModelAndView("update","message",updateController.updateInfo());
	}
	
	
	@RequestMapping(path = "/users", method = RequestMethod.GET)
	public ModelAndView getOneUser(@RequestParam("isstr") String isstr, @RequestParam("month") Long month){
		response = monthController.getAllUsers(isstr, month);
		if(response.getTypeResponse()=="200") {
	        return new ModelAndView("singleMonthView", "months", response.getResult()); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
		}else {
			return new ModelAndView("error");
		}
	}
	
	@RequestMapping(path = "/period", method = RequestMethod.GET,produces = "application/json")
	public ModelAndView getAllUsers(@RequestParam("month") Long month){
		response = monthController.getPeriod(month);
		if(response.getTypeResponse()=="200") {
			return new ModelAndView("monthView", "months", response.getResult()); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
		}else {
			return new ModelAndView("error");

		}
	}
	/*
	@RequestMapping(path = "/users/{isstr}/{month}", method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody ModelAndView postEmployeeData(Model model) {
		Month person =  monthService.getbyIsAndByIMonth(isstr, month);
        model.addAttribute("persons", dayController.getUsers("2019-01-01", "2019-01-31"));
        return new ModelAndView("personList", "persons", persons); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
	}*/
	
}
