package com.softtek.academy.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.models.Day;
import com.softtek.academy.models.Response;
import com.softtek.academy.services.DayService;

@Controller
@RequestMapping("view/api/v1")
public class DayViewController {
	
	@Autowired
	DayService dayService;
	
	@Autowired
	DayController dayController;
	
	Response response;
	
	@RequestMapping(value = "/personList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ModelAndView postEmployeeData() {
		List<Day> persons = dayService.getAllUsers();
        return new ModelAndView("dayView", "persons", persons); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
	}
	
	
	@RequestMapping(value = "/period", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ModelAndView getByDates(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate) {
		response = dayController.getUsers(startDate, endDate);
		if(response.getTypeResponse()=="200") {
	        return new ModelAndView("dayView", "persons", response.getResult()); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
		}else {
			return new ModelAndView("error");
		}
	}
	
	
	@RequestMapping(value = "/users", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ModelAndView getUserByIsAndDates(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate, @ModelAttribute("is") String is) {
		response = dayController.getUsers(startDate, endDate,is);
		if(response.getTypeResponse()=="200") {
			return new ModelAndView("dayView", "persons",response.getResult()); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
		}else {
			return new ModelAndView("error");
		}
	}	
}
