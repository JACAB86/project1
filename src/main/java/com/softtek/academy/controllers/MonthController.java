package com.softtek.academy.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.models.Month;
import com.softtek.academy.models.Response;
import com.softtek.academy.services.MonthService;

@RestController
@RequestMapping("api/v1")
public class MonthController {
	@Autowired
	MonthService monthService;

	@RequestMapping(path = "/users/{isstr}/{month}", method = RequestMethod.GET,produces = "application/json")
	@ResponseBody
	public Response getAllUsers(@PathVariable String isstr, @PathVariable Long month){
			return monthService.getbyIsAndByIMonth(isstr, month);

	}
	
	@RequestMapping(path = "/period/{month}", method = RequestMethod.GET,produces = "application/json")
	@ResponseBody
	public Response getPeriod(@PathVariable Long month){
		return monthService.getAllByIMonth(month);
	}
}
