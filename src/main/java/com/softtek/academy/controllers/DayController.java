package com.softtek.academy.controllers;

import java.util.List;

import org.aspectj.apache.bcel.classfile.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.softtek.academy.models.Day;
import com.softtek.academy.models.Month;
import com.softtek.academy.models.Response;
import com.softtek.academy.services.DayService;

import javassist.NotFoundException;


@RestController
@RequestMapping("api/v1")
public class DayController {

	@Autowired
	DayService dayService;
	

	@RequestMapping(path = "/users", method = RequestMethod.POST)
	public Response getUsers(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate, @ModelAttribute("is") String is){
			return dayService.getUsers(is,startDate, endDate);

	}
	
	@RequestMapping(path = "/period", method = RequestMethod.POST)
	public Response getUsers(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate){
		return dayService.getBetweenDates(startDate, endDate);
	}
	
	/*
    @RequestMapping(value = { "/personList" }, method = RequestMethod.GET)
    public ModelAndView viewPersonList(Model model) {
 
    	List<Day> persons = dayService.getAllUsers();
        model.addAttribute("persons", persons);
        return new ModelAndView("personList", "persons", persons); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
    }*/
	
    
	@RequestMapping(value = "/personList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ModelAndView postEmployeeData(Model model) {
		List<Day> persons = dayService.getAllUsers();
        model.addAttribute("persons", persons);
        return new ModelAndView("personList", "persons", persons); // Nombre JSP / Nombre que se le dara a lo que reciba // lo que le enviare
	}
    
	
    
    /*
     * @GetMapping("/rates")
public String handleForexRequest(Model model, HttpServletResponse response) {
    response.setHeader("Content-Disposition", "attachment; filename=rates.json");
    model.addAttribute("currencyRates", getCurrencyRates());
    return "currencyRateView";
}
     * 
     * */
}
