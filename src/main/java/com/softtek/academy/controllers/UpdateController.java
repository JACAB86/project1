package com.softtek.academy.controllers;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.services.UpdateService;


@RestController
@RequestMapping("api/v1")
public class UpdateController {
	
	@Autowired
	private UpdateService updateService;
	
    @RequestMapping("/update")
    public String updateInfo() throws ParseException, IOException {
    	updateService.updateAll();
        return "Congratulations on updating the database";
    }

}
